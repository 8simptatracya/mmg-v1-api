<?php

namespace App\Models;

use Backpack\CRUD\app\Models\Traits\CrudTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    use CrudTrait;
    use HasFactory;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'name',
                  'task_definition_id',
                  'plant_id',
                  'description',
                  'date_beg',
                  'date_end',
                  'zone_id'
    ];

    /**
     * Get the Task Definition for this model.
     *
     * @return App\Models\TaskDefinition
     */
    public function task_definition()
    {
        return $this->belongsTo('App\Models\TaskDefinition','task_definition_id','id');
    }

    /**
     * Get the Plant for this model.
     *
     * @return App\Models\Plant
     */
    public function plant()
    {
        return $this->belongsTo('App\Models\Plant','plant_id','id');
    }

     /**
     * Get the Zone for this model.
     *
     * @return App\Models\Zone
     */
    public function zone()
    {
        return $this->belongsTo('App\Models\Zone','zone_id','id');
    }
}
